# User Directory
> 20/02/2020 – INTERVIEW ASSIGNMENT - VARIANT B

This project is designed to demonstrate a your ability to build a simple web app and communicate with an API. 

## Getting started
1. Fork this repository (you will need a GitLab account)
2. Read `frontend/readme.md` to understand the task
3. Read `backend/readme.md` to understand the API (note: you are not required to make any changes to the code of the backend server).
4. Complete the assignment
5. Submit a Merge Request back to the original GitLab repo
